# Notes for 12 Sep

## Goals
HDK want to increase their productivity, meaning:
Every task should be completed on time and with good quality.

## Requirements from customer interview

- Keep track of tasks and employees
    - What information do you need about tasks/employees? (input data)
- Provides statistics of tasks being completed, missed deadlines etc.
    - What information do you need and why? (output data)
    - How would you use these information and what for?
- Easy to use
    - How do you define easy to use? How would this be measured?
- A way of communicating inside of the system
    -  Why is this needed, in what way does this help you reach the goal?
    -  How should this happen? (chat systems? comments?)
- The ability to sort tasks on different status
    - Why? Is this related to statistics?
- The ability to label different tasks
    - is it the same as creating new tasks? (description - input data?)
- The ability to manage tasks
    - Who is managing the tasks?
    - What is managing tasks? (distibuting tasks or assigning a status?)
- Different interfaces depending on the users authorization level
    - Which levels? and how many levels?
    - Who are using the system?
    - In what way would the interfaces differ?
- The ability to upload/download files from the system
    - What kind of files should the system support?
    - Who should be able to upload/download files from the system?
    - What about Security?
- Personal accounts for every user
    - What information should the system store about the users? (input data)
    - Mandatory vs. Optional information? (input data)
    - Should different stackholders have different kind of information?

### Negotiate
- Keeps track of employees
    - In what way do you want to keep track of employees? (how and why)
    - How does that help you reach the goal?
- Works on different platforms
    - In what way would this solve the problem of not completing tasks? (why)
- Some sort of touch screen should be placed on every work station
    - is that a requirement?
    - is this a restriction?


## Questions

- Can you provite example tasks that should be handled by the system? Ideally - Worst case Senario.
- If there is a big task which can be divided into small tasks tied to different staffs, how can the system manage them (time, maybe delay affects other tasks)?
- What can teamleader do in the system (just same as employees? what are his tasks?)


## Requirements from workshop
- Predict all the things
    - Recuring tasks should be predicted/automated
    - Let managers add recuring tasks
- Security
    - Protecting privileges? (cheating)
    - How to identify the user? (input - username and password only? biometrics?)
    - How to handle the login? (input - username and password only? biometrics?)
    - Backup and synchronization of data to HQ? (recovery)
    - what is the risks of data loss? (does that affect the goal?)
- Automate report loop
    - gather and offer all the data (Regualar reports?)
- Stats comparing employees and restaurants
    - Maybe give bonuses to the best or use the info to improve the process where there are problems.
    - This might incentivize better performance.
    - is this related to statistics?
- Quality control / micromanage
    - Clear instructions about how to do the tasks (Input and output data)
- equal workload
    - Should the system provide some kind of recommendation on who to assign different tasks?
    - would that be a problem for statistics or comparing employees?
- feedback and comments on tasks
    - Is this part of the communication system?
- everything is replaceable
    - can the tasks be re-assigned?
    - In what way should actors be able to interact with the tasks (other than touch screens)?
- Tasks inside of tasks
    - Worst case scenario? (what is the most complicated case of tasks)
    - Does different task contain other tasks? (Sub-tasks)
    - Does tasks overlap? relate? (Priority or certain sequence)?
- Have reminders
    - If tasks gets forgotten?
    - If a task is created and not assigned?
    - How should the system provide these reminders if any?
- Dedicated hardware?
    - do you need alternatives to touch screens / cross-platform thing?
    - with dedicated accouts? what about security? how do we keep track?
- How to make sure that an employee completed the assigned task?
    - would the employee ask his friends to complete his tasks? (cheating)


