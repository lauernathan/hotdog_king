# Interview with customer group

## Background

- What is the main problem?
    - hard to keep track of all the tasks; cleaning, cooking, etc. We want to structure it. What's supposed to be done. It shoudld be used by managars, keeping track of tasks. Now we assign tasks by phone and stuff.
- What tasks do you have
    - It should be for management and "clean aile 3"
- What is the problem with the tasks
    - Tasks that are never being done?
        - Two persons can do the same thing, some tasks are forgotten.
        - To keep track of the employees. Who did what and when.
    - Tasks descriptions?
    - What do you mean with "structure"?
        - Today: word of mouth
- How would an it system help?
    - Management isnt around all the time. To keep track of tasks to do. Deadlines, scheduling. Who should do what. Easy to assign and keep track.
- How manage today?
    - Checklists, word of mouth, no real structure.

## Goal

- How would this system increase the productivity
    - In the future the machines should be able to automatically ask for maintenance.
- How would the system encourage agile workflow
    - Better planning, communication, restructuring tasks more rapidly.
- What is simplicity for you?
    - intuitive, easy to use, all employees have different knowledge, self explanatory
- What "work related" information is there
    - personal account with work descriptions, contact details, which position.
    -
- How would the system be a total success
    - Integrate this system in all franchises
    - More efficient processes
    - Don't forget to do stuff
- What kind of system=
    - All platforms, web app. and mobile app, touch support, should be placed in every work station

## Roles/Stakeholders

- Who uses the system in what way?
    - HQ, franchises, managers, employees
    - Three levels system
- Who defines and assignes the tasks?
    - Managers creates and assigns tasks, no one else should be able to do this.
    - Tasks are assign on a team-level
- Who performs the task?
    - Team leader shouldn't add tasks but make sure that everything is being completed
    -
- Will there be customer interaction with the system
    - At the moment customers should not be able to interact with the system.
- For whom will the system be?
    - Managers, HR, chefs, customers?
- How will the system handle different regulations from government?
    - Maybe use some lables to flag different tasks needing certain certifications

## Risks

- Do you see any risks with this system?
    - Downtime and dependency,
- Advantages or disadvantages compared to now?

## Functionality
- Give us an overview of the current process.
    - Word of mouth, spreadsheets (see above)
- Filtering system
    - Different deadlines
    - Finished tasks
    - priorities
- Chefs have a screen that displays everything
- Chat function for employee - manager
- Interested in statistics
    - num completed tasks, num assigned tasks, missed deadlines
    - visual presentation
- Not interested in payment system
- Different interfaces / access levels for different levels in the hierarchy.
- The files uploaded should be for supporting the tasks.