# 13, Sep Customer Meeting

- Information about employees
    - Task should have: title, description what it needs, who is assigned, who created, deadline, location (restaurant and area)
    - Tasks have Labels or categories: cleaning, construction, maintenance .. etc
    - Employees: id, name, photo, role description, contact information, which restaurant, positions
    - Employees are already saved in db.

- statistics
    - for hq to find out information how many tasks are completed, how many failed, use to improve the working situation and improve processes
    - By how long time was the deadline missed, which days, in which areas
    - Is a certain always missing deadlines
    - stats about emps

- Communication in the system
    - Some way to ask for more information about the tasks
    - If a task can't be done then give a reason for why
    - comments should be available on tasks as a way of communication

- Stakeholders
    - Three levels: employees, manager, hq
    - employees should see, comment, complete tasks. Only see the the lower tier tasks. Only in their own restaurant
    - Managers: they can create tasks, create tasks for their own level, assign tasks to people, assign a status and level. They can only see their own level, and the lower level. They should see statistics for their own restaurants.
    - HQ: assign tasks to everyone, status to tasks, label. Watch all the tasks in every restaurant. Create tier 1 task. Statistics from every restaurant


- systems
    - Database for employees that we should use
    - Employees identify themselfs with id cards
    - Payment system
    - A database is needed for the tasks

- upload and download files
    - employees shouldn't use it
    - managers should be able to upload pdfs, excel files, photos, etc
    - HQ uploads new guidelines for xy
    - security issues: not super important, we trust the employees

- personal accounts for users
    - some kind of profiles
    - login with id card

- easy to use
    - basic application, intuitive
    - measure by forms to the employees
    - usability is critical
    - one or two clicks should be enough to complete a task
        - As an employee get a list of your tasks with your id card and hit complete

- devices and platforms
    - you work at a station and are logged in. The system notifies the employees
    - dedicated hardware only (no phones) even if the employee was busy
    - voice recognition might be hel
- other
    - manager can re-assign tasks
    - tasks can be assigned to multiple people.
    - one instance of task for multiple people and multiple inctances of the same task for multiple people.
    - tasks has status (open, assigned, in progress, completed)
    - ID cards are enough to prevent cheating
    - ID cards are enough to identify users
    - managers have no access to see the statistics of other restaurants bu HQ can see all info

- control of employees
    - Schedule? System should show which employees are logged in

- related tasks
    - reference one task to another one just for ordering thing.
    - Not be able to complete some tasks before a referenced task is completed

- automated reports on a weekly and monthly basis
    - Compared to other months and years

- how bad is data loss or break downs
    - no vital information should be lossed
    - there would be time loss
    - never be down for more than 2 days
    - recovery is important for statistics

- when to synchronize
    - in real-time

- how many restaurants / tasks / staff
    - 220 restaurants and a 10% increase each year
    - 20 tasks per restaurant at the same time
    - 8-10 stations per restaurant at the same time
    - 220 managers
    - 500 hq

- prediction
    - System could suggest tasks for the manager
    - Manager should be able to set recurring tasks

- forgotten tasks
    - You have to assign a task to an employee or a team, otherwise you can't create the task
















