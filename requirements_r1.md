# Requirements

- The system should have the ability to create new tasks
    - For Managers (within the manager's own level and branches or below)
        - The system shall provide the ability to add all needed data to tasks **TODO**: Be more specific about what data
        - The system shall provide the ability to add labels to tasks
        - The system shall support uploading/downloading document **Also pictures eg.** files to tasks
        - The system shall provide a way to set **one or multiple** prerequisite task to tasks
        - the system shall not complete adding a new task without assigning it first to one or more employees **employee is a role, use user instead**
        - The system should store the new tasks in db
    - For HQ (within all levels and branches)
        - The system shall provide the ability to add all needed data to tasks  **TODO**: Be more specific about what data
        - The system shall provide the ability to add labels to tasks
        - The system shall support uploading/downloading document files to tasks **Also pictures eg.**
        - The system shall provide a way to set **one or multiple** prerequisite task to tasks
        - The system shall not add a task without assigning it first to one or more employees **employee so far is a role so use user instead**
        - The system should store the new tasks in db

-----------------------------------------

- The system should have the ability to update tasks
    - For Managers (within the manager's own level and branches or below)
        - The system shall provide the ability to update all needed data to tasks **Again specify which data**
        - The system shall provide the ability to **add and remove** labels **from** tasks
        - The system shall support uploading/downloading document files to tasks **pictures eg**
        - The system shall prohibit unassigning tasks but only re-assigning them to one or more employee
        - The system shall provide a way to **add and remove** prerequisite tasks to tasks
        - The system should update the stored the tasks in db
    - For HQ (within all levels and branches)
        - The system shall provide the ability to update all needed data to tasks **what data**
        - The system shall provide the ability to update labels of tasks  **add/remove**
        - The system shall support uploading/downloading document files to tasks  **pictures**
        - The system shall provide a way to update a prerequisite task to tasks **add/remove**
        - The system shall prohibit un-assigning tasks but only re-assigning to one or more employee
        - The system should update the stored tasks in db

-----------------------------------------

- The system should have the ability to delete tasks (any restriction by status?)
    - For managers (within the manager's own level and branches or below)
        - The system shall provide the ability to delete tasks
    - For HQ (within all levels and branches)
        - The system shall provide the ability to delete tasks

-----------------------------------------

- The system shall provide the ability to set a list of recurring tasks**without activating it (see next block)**
    - For Managers (within the manager's own level and branches or below)
        - The system shall provide the ability to add new tasks to the list without assigning each one of them  **why not assign them**
    - For HQ (within all levels and branches)
        - The system shall provide the ability to add new tasks to the list without assigning each one of them  **why not assign them**

-----------------------------------------

- The system shall provide the ability to activate previously available recurring task list
    - For Managers (within the manager's own level and branches or below)
        - The system should force assigning each task of the list to one or more employees before activating it
    - For HQ (within all levels and branches)
        - The system should force assigning each task of the list to one or more employees before activating it

-----------------------------------------

- The system should provide the ability to comment on tasks
    - For Employees (for the employee's tasks only)
        - The ability to add comment on tasks
        - Update/Delete? **not needed**
    - For Managers (within the manager's own level and branches or below)
        - The ability to add comment on tasks
        - Update/Delete? **not needed**
    - For HQ (within all levels and branches)
        - The ability to add comment on tasks
        - Update/Delete? **not needed**

-----------------------------------------

- The system shall provide a login feature
    - For Employees / For Managers / For HQ
        - The system shall request identification for each time they use the system **using the id card**
        - **The system shall provide the user with the view corresponding to the privileges of the id card.**

-----------------------------------------

- The system shall predict recurring tasks
    - For Managers (within the manager's own level and branches or below)
        - The system shall predict recurring tasks based on previous tasks
    - For HQ (within all levels and branches)
        - the system shall predict recurring tasks based on previous tasks

-----------------------------------------

- The system shall provide a statistical overview regarding the tasks
    - For Managers (within the manager's own level and branches or below)
        - The system shall provide information about tasks according the their status
        - The system should have the ability sort the tasks according the their status
        - The system shall provide information about tasks according the their deadline
        - The system should have the ability sort the tasks according the their deadline
        - The system shall sort the statistics according to different view-modes: by task, by employees,by restaurant, **by time frame**
        - The system shall provide real data about tasks
        - The system shall provide information about online users
    - For HQ (within all levels and branches)
        - The system shall provide information about tasks according the their status
        - The system should have the ability sort the tasks according the their status
        - The system shall provide information about tasks according the their deadline
        - The system should have the ability sort the tasks according the their deadline
        - The system shall sort the statistics according to different view-modes: by task, by employees,by restaurant, **by time frame**
        - The system shall provide real data about tasks
        - The system shall provide information about online users

-----------------------------------------

- The system shall provide reports about statistical information over a period of time (month / year)
    - For Managers (within the manager's own level and branches or below)
        - The system should provide a detailed report based on tasks **matching provided time frames**
    - For HQ (within all levels and branches)
        - The system should provide a detailed report based on tasks **matching provided time frames**

-----------------------------------------

- The system shall provide notification to users about tasks (? more information from customer)
    - For Employees (within the employee's own level and branch)
        - The system should provide notification about newly assigned and/or updated tasks
    - For Managers (within the manager's own level and branches or below)
        - The system should provide notification about newly assigned and/or updated tasks
    - For HQ (within all levels and branches)
        - The system should provide notification about newly assigned and/or updated tasks

-----------------------------------------

- The system shall provide the ability to list tasks related to users
    - For Employees (within the employee's own level and branch)
        - The system shall not list the tasks without ID verification **redundant with the login requ**
        - The system shall list tasks
    - For Manager (within the manager's own level and branches or below)
        - The system shall not list the tasks without ID verification (? further investigation) **redundant with the login requ**
        - The system shall list tasks
    - For HQ (within all levels and branches)
        - The system shall not list the tasks without ID verification (? further investigation) **redundant with the login requ**
        - The system shall list tasks

-----------------------------------------

- The system shall provide the ability to Switch the status of tasks
    - For Employees (for the employee's tasks only)
        - The system shall provide the ability to set the status of tasks to "In-Progress" after listing them
            - Only for tasks of status: New/Open/Assigned
        - The system shall provide the ability to set the status of tasks to "Complete" after listing them
            - Only for tasks of status: In-Progress that have no prequisite task or have a completed prerequisite task **all, not just one**
        - The system shall provide the ability to set the status of tasks to "Failed" after listing them
            - Only for tasks of status: In-Progress that have no prerequisite task or have a completed prerequisite task **for all tasks?**
    - For Managers (within the manager's own level and branches or below)
        - The system shall provide the ability to set the status of tasks to "In-Progress" after listing them
            - Only for tasks of status: New/Open/Assigned
        - The system shall provide the ability to set the status of tasks to "Complete" after listing them
            - Only for tasks of status: In-Progress that have no prequisite task or have a completed prequisite task **all, not just one**
        - The system shall provide the ability to set the status of tasks to "Failed" after listing them
            - Only for tasks of status: In-Progress that have no prequisite task or have a completed prequisite task **for all tasks?**
    - For HQ (within all levels and branches)
        - The system shall provide the ability to set the status of tasks to "In-Progress" after listing them
            - Only for tasks of status: New/Open/Assigned
        - The system shall provide the ability to set the status of tasks to "Complete" after listing them
            - Only for tasks of status: In-Progress that have no prequisite task or have a completed prequisite task **all, not just one**
        - The system shall provide the ability to set the status of tasks to "Failed" after listing them
            - Only for tasks of status: In-Progress that have no prequisite task or have a completed prequisite task **for all tasks?**

-----------------------------------------

# Data
- The task attributes are:
    -  title
    -  description
    -  holder/holders (user-ids)
    -  creator (user-id)
    -  deadline
    -  location-id
    -  label-id
    -  status-id
    -  comment-ids
    -  file-ids

- The task labels are:
    - For Employees
        - repairs
        - cleaning
        - general changes
        - improvement
    - For Managers
        - employees management
        - restaurant management
    - For HQ
        -  employees management
        -  restaurant management
        -  manager management
        -  company management
        -  business
        -  marketing

- The task status are:
    - open/new (only for recurring list)
    - assigned
    - in progress
    - completed
    - failed

- Employee attributes:
    - Role-id
    - ID to link it to Employee DB that has the rest of info:
        - name
        - photo
        - description
        - contact information
        - restaurant

------------------------

## Quality function

- Scalability
    - The system should be able to deal with 250 restaurants with an increase each year with 10%

- Performance
    - The system should be able to handle 20 tasks at the same time (changes)
    - The system should handle around 10 stations/restaurant
    - The system shall service 220 managers, 500 hq, 20(employees) X 220(restaurants), in total (~5200 employees).
    - The system should be able to synchronize all data in real time

- Usability
    - The system should be user-friendly ,intuitive  and basic by providing each service within 2 to 3 clicks.

- Reliability
    - The system shall not permit data loss (use logs)
    - The system shall recover after each problem (using logs)

- Availability
    - The system should be online all the time (offline for max 2 days)


​